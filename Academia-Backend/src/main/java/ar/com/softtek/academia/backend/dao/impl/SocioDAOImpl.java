package ar.com.softtek.academia.backend.dao.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.dao.DataAccessException;

import ar.com.academia.dto.SocioMapper;
import ar.com.academia.entities.Socio;
import ar.com.academia.entities.SocioDTO;
import ar.com.academia.excepciones.PersistenceException;
import ar.com.softtek.academia.backend.dao.SocioDAO;

public class SocioDAOImpl extends GenericDAOImpl<Socio> implements SocioDAO{
	
	@Override
	public Class<Socio>getType(){
		return Socio.class;
	}
	
	public List<SocioDTO> getAllSocios()throws PersistenceException
	{
		try
		{
			List<Socio> lista = this.getAll();
			
			
			return SocioMapper.convertSocioListToDTOList(lista);
		}catch(PersistenceException e)
		{
			throw new PersistenceException(e);
		}
	}
	
	public List<Socio> getAll() throws PersistenceException{
		try{
			List<Socio> lista = super.getAll();
			return lista;
		}
		catch(PersistenceException e)
		{
			throw new PersistenceException(e);
		}
	}
	
	public SocioDTO getSocioById(int id) throws PersistenceException
	{
		try
		{
			SocioDTO dto = SocioMapper.convertSocioToDTO(this.getById(id));
			return dto;
		}
		catch(PersistenceException e)
		{
			throw new PersistenceException(e);
		}
	}
	
	public Socio getById(int id) throws PersistenceException{
		try
		{
			Socio socio = super.getById(id);
			return socio;
		}
		catch(PersistenceException e)
		{
			throw new PersistenceException(e);
		}
	}
	
	public void save(SocioDTO socioDTO) throws PersistenceException{
		try
		{
			Socio socio = SocioMapper.convertDTOtoSocio(socioDTO);
			super.save(socio);
		}
		catch(PersistenceException e)
		{
			throw new PersistenceException(e);
		}
	}
	
	public void update(SocioDTO socioDTO) throws PersistenceException{
		try
		{
			Socio socio = SocioMapper.convertDTOtoSocio(socioDTO);
			super.update(socio);
		}
		catch(PersistenceException e)
		{
			throw new PersistenceException(e);
		}
	}
	
	public boolean delete(SocioDTO socioDTO) throws PersistenceException{
		boolean isRemoved = false;
		try
		{
			Socio socio = SocioMapper.convertDTOtoSocio(socioDTO);
			super.delete(socio);
			isRemoved = true;
		}
		catch(DataAccessException e)
		{
			throw new PersistenceException(e);
		}
		
		return isRemoved;
	}
	
	public boolean delete(int id) throws PersistenceException{
		boolean isRemoved = false;
		try
		{
			super.delete(id);
			isRemoved = true;
		}
		catch(HibernateException e)
		{
			throw new PersistenceException(e);
		}
		
		return isRemoved;		
	}
	
	public int count() throws PersistenceException{
		try{
			Session session = this.getSessionFactory().getCurrentSession();
			Criteria criteria = session.createCriteria(Socio.class);
			criteria.setProjection(Projections.rowCount());
			int count = (int) criteria.uniqueResult();
			return count;
		}
		catch(HibernateException e)
		{
			throw new PersistenceException(e);
		}
	}
	
	public List<SocioDTO> getBySexo(String sexo)throws PersistenceException{
		
		try
		{	
		
			Session session = getSessionFactory().getCurrentSession();
			Criteria criteria = session.createCriteria(Socio.class);
			criteria.add(Restrictions.eq("sexo", sexo));
			List<Socio> result = criteria.list();
			
			return SocioMapper.convertSocioListToDTOList(result);
		}
		catch(HibernateException e)
		{
			throw new PersistenceException(e);
		}
		
	}

	
}
