package ar.com.softtek.academia.backend.dao.impl;

import java.util.List;

import ar.com.academia.entities.Turno;
import ar.com.academia.excepciones.PersistenceException;
import ar.com.softtek.academia.backend.dao.TurnoDAO;

public class TurnoDAOImpl extends GenericDAOImpl<Turno> implements TurnoDAO{

	
	public List<Turno> getAll()throws PersistenceException
	{
		return null;
	}
	
	public Turno getById(int it)throws PersistenceException
	{
		return null;
	}
	
	public boolean delete(int id)throws PersistenceException
	{
		return false;
	}
	
	public boolean delete(Turno prestador)throws PersistenceException
	{
		return false;
	}
	
	public void save(Turno prestador)throws PersistenceException
	{
		
	}
	
	public void update(Turno prestador)throws PersistenceException
	{
		
	}
	
	public int count()throws PersistenceException
	{
		return 0;
	}
	
	
	
}
