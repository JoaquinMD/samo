package ar.com.softtek.academia.backend.bo;

import java.util.List;

import ar.com.academia.entities.TurnoDTO;
import ar.com.academia.excepciones.BusinessException;

public interface TurnoBO {

	public void actualizarTurno(TurnoDTO turnoDTO) throws BusinessException;
	
	public boolean eliminarTurno(TurnoDTO turnoDTO) throws BusinessException;
	
	public void crearTurno(TurnoDTO turnoDTO) throws BusinessException;
	
	public List<TurnoDTO> getAllTurnos() throws BusinessException;
	
	public TurnoDTO getTrunoById(int id) throws BusinessException;
	
}
