package ar.com.softtek.academia.backend.bo.impl;

import java.util.List;

import ar.com.academia.entities.TurnoDTO;
import ar.com.academia.excepciones.BusinessException;
import ar.com.softtek.academia.backend.bo.TurnoBO;
import ar.com.softtek.academia.backend.dao.TurnoDAO;

public class TurnoBOImpl implements TurnoBO{
	
	private TurnoDAO turnoDAO;


	@Override
	public void actualizarTurno(TurnoDTO turnoDTO) throws BusinessException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean eliminarTurno(TurnoDTO turnoDTO) throws BusinessException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void crearTurno(TurnoDTO turnoDTO) throws BusinessException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<TurnoDTO> getAllTurnos() throws BusinessException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TurnoDTO getTrunoById(int id) throws BusinessException {
		// TODO Auto-generated method stub
		return null;
	}

	public TurnoDAO getTurnoDAO() {
		return turnoDAO;
	}

	public void setTurnoDAO(TurnoDAO turnoDAO) {
		this.turnoDAO = turnoDAO;
	}
}
