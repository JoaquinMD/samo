package ar.com.softtek.academia.backend.dao;

import java.util.List;

import ar.com.academia.entities.Socio;
import ar.com.academia.entities.SocioDTO;
import ar.com.academia.excepciones.PersistenceException;

public interface SocioDAO extends GenericDAO<Socio>{
	
	public List<SocioDTO> getBySexo(String sexo)throws PersistenceException;
	
	public SocioDTO getSocioById(int id)throws PersistenceException;
	
	public List<SocioDTO> getAllSocios()throws PersistenceException;
	
	public void update(SocioDTO socioDTO) throws PersistenceException;
	
	public boolean delete(SocioDTO socioDTO) throws PersistenceException;
	
	public void save(SocioDTO socioDTO) throws PersistenceException;
}
