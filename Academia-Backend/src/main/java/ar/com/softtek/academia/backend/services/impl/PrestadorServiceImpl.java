package ar.com.softtek.academia.backend.services.impl;

import java.util.List;

import javax.jws.WebService;

import ar.com.academia.entities.PrestadorDTO;
import ar.com.academia.excepciones.ServiceException;
import ar.com.academia.services.PrestadorService;

@WebService(endpointInterface="ar.com.academia.services.PrestadorService", serviceName="PrestadorService")
public class PrestadorServiceImpl implements PrestadorService {
	
	//private PrestadorBO prestadorBO;
	
	public int add(PrestadorDTO prestadorDTO)throws ServiceException
	{
		return 0;
	}
	
	public boolean edit(PrestadorDTO prestadorDTO)throws ServiceException
	{
		return false;
	}
	
	public boolean removeById(int id)throws ServiceException
	{
		return false;
	}
	
	public List<PrestadorDTO> getAll()throws ServiceException
	{
		return null;
	}
	
	public PrestadorDTO getById(int id) throws ServiceException
	{
		return null;
	}
	
	/*
	public PrestadorBO getPrestadorBO(){
		return prestadorBO;
	}
	
	public void setPrestadorBO(PrestadorBO prestadorBO)
	{
		this.prestadorBO = prestadorBO;
	}
	*/

}
