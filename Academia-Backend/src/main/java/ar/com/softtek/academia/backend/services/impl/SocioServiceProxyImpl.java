package ar.com.softtek.academia.backend.services.impl;

import java.util.List;

import javax.jws.WebService;

import ar.com.academia.entities.SocioDTO;
import ar.com.academia.excepciones.ServiceException;
import ar.com.academia.services.SocioService;
import ar.com.academia.services.SocioServiceProxy;

@WebService(endpointInterface="ar.com.academia.services.SocioServiceProxy", serviceName="SocioServiceProxy")
public class SocioServiceProxyImpl implements SocioServiceProxy{

	SocioService service;
	
	public int add( SocioDTO socioDTO)throws ServiceException {
		try
		{
			int id = service.add(socioDTO);
			return id;
		}
		catch(ServiceException se)
		{
			throw new ServiceException();
		}
	}

	
	public List<SocioDTO> getAll() throws ServiceException {
		try
		{
			List<SocioDTO> lista = service.getAll();
			return lista;
		}
		catch(ServiceException se)
		{
			throw new ServiceException();
		}
	}

	
	public SocioDTO getById( int id) throws ServiceException {
		try
		{
			SocioDTO socio = service.getById(id);
			return socio;
		}
		catch(ServiceException se)
		{
			throw new ServiceException();
		}
	}

	
	public List<SocioDTO> getBySexo( String sexo)
			throws ServiceException {
		try
		{
			List<SocioDTO> lista = service.getBySexo(sexo);
			return lista;
		}
		catch(ServiceException se)
		{
			throw new ServiceException();
		}
	}

	
	public boolean removeById( int id)
			throws ServiceException {
		try
		{
			boolean isRemoved = service.removeById(id);
			return isRemoved;
		}
		catch(ServiceException se)
		{
			throw new ServiceException();
		}
	}
	
	
	public SocioService getService()
	{
		return service;
	}
	
	public void setService(SocioService service)
	{
		this.service = service;
	}

}
