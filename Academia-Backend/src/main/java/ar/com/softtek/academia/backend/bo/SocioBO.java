package ar.com.softtek.academia.backend.bo;

import java.util.List;

import ar.com.academia.entities.SocioDTO;
import ar.com.academia.excepciones.BusinessException;

public interface SocioBO {
	
	public void actualizarSocio(SocioDTO socioDTO) throws BusinessException;
	
	public boolean eliminarSocio(SocioDTO socioDTO) throws BusinessException;
	
	public boolean eliminarSocio(int id) throws BusinessException;
	
	public int cantidadSocios() throws BusinessException;
	
	public void crearSocio(SocioDTO socioDTO) throws BusinessException;
	
	public List<SocioDTO> getAllSocios() throws BusinessException;
	
	public SocioDTO getSocioById(int id) throws BusinessException;

}
