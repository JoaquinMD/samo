package ar.com.softtek.academia.backend.services.impl;

import java.util.List;

import javax.jws.*;

import ar.com.academia.entities.SocioDTO;
import ar.com.academia.excepciones.BusinessException;
import ar.com.academia.excepciones.ServiceException;
import ar.com.academia.services.SocioService;
import ar.com.softtek.academia.backend.bo.impl.SocioBOImpl;

@WebService(endpointInterface="ar.com.academia.services.SocioService", serviceName="SocioService")
public class SocioServiceImpl implements SocioService{

		private SocioBOImpl socioBO;
		
		public int add(SocioDTO socioDTO)throws ServiceException
		{
			try
			{
				getSocioBO().crearSocio(socioDTO);
				return 0;
			}
			catch(BusinessException be)
			{
				throw new ServiceException(be);
			}
		}
		
		public List<SocioDTO> getAll()throws ServiceException
		{
			try
			{
				List<SocioDTO> lista = getSocioBO().getAllSocios();
				return lista;
			}
			catch(BusinessException be)
			{
				throw new ServiceException(be);
			}
		}
		
		public SocioDTO getById(int id)throws ServiceException
		{
			try
			{
				SocioDTO socio = getSocioBO().getSocioById(id);
				return socio;
			}
			catch(BusinessException be)
			{
				throw new ServiceException(be);
			}
		}
		
		public List<SocioDTO> getBySexo(String sexo)throws ServiceException
		{
			try
			{
				List<SocioDTO> lista= getSocioBO().getSociosBySexo(sexo);
				return lista;
			}
			catch(BusinessException be)
			{
				throw new ServiceException(be);
			}
		}
		
		public boolean removeById(int id)throws ServiceException
		{
			try
			{
				boolean isRemoved = getSocioBO().eliminarSocio(id);
				return isRemoved;
			}
			catch(BusinessException be)
			{
				throw new ServiceException(be);
			}
		}
		
		public int count() throws ServiceException
		{
			try
			{
				int count = getSocioBO().cantidadSocios();
				return count;
			}
			catch(BusinessException be)
			{
				throw new ServiceException(be);
			}
		}
		
		public boolean edit(SocioDTO socioDTO)throws ServiceException
		{	
			boolean isEdited = false;
			try
			{
				getSocioBO().actualizarSocio(socioDTO);
				isEdited = true;
			}catch(BusinessException e)
			{
				throw new ServiceException(e);
			}
			return isEdited;
		}
		
		public SocioBOImpl getSocioBO(){
			return socioBO;
		}
		
		public void setSocioBO(SocioBOImpl socioBO){
			this.socioBO = socioBO;
		}
}
