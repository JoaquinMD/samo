package ar.com.softtek.academia.backend.services.impl;

import java.util.List;

import ar.com.academia.entities.TurnoDTO;
import ar.com.academia.excepciones.ServiceException;
import ar.com.academia.services.TurnoService;
import ar.com.softtek.academia.backend.bo.TurnoBO;

import javax.jws.WebService;

@WebService(endpointInterface="ar.com.academia.services.TurnoService", serviceName="TurnoService")
public class TurnoServiceImpl implements TurnoService {

	private TurnoBO turnoBO;
	
	public int add(TurnoDTO turno)throws ServiceException
	{
		return 0;
	}
	
	public boolean edit(TurnoDTO turno)throws ServiceException
	{
		return false;
	}
	
	public boolean removeById(int id)throws ServiceException
	{
		return false;
	}
	
	public List<TurnoDTO> getByIdSocio(int id)throws ServiceException
	{
		return null;
	}
	
	public TurnoDTO getById(int id) throws ServiceException
	{
		return null;
	}
	
	public TurnoBO getTurnoBO(){
		return turnoBO;
	}
	
	public void setTurnoBO(TurnoBO turnoBO){
		this.turnoBO = turnoBO;
	}
	
	
	
}
