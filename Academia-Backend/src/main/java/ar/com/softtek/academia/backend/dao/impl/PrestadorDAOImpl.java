package ar.com.softtek.academia.backend.dao.impl;

import java.util.List;

import ar.com.academia.entities.Prestador;
import ar.com.academia.excepciones.PersistenceException;
import ar.com.softtek.academia.backend.dao.PrestadorDAO;

public class PrestadorDAOImpl extends GenericDAOImpl<Prestador> implements PrestadorDAO {
	
	public List<Prestador> getAll()throws PersistenceException
	{
		return null;
	}
	
	public Prestador getById(int it)throws PersistenceException
	{
		return null;
	}
	
	public boolean delete(int id)throws PersistenceException
	{
		return false;
	}
	
	public boolean delete(Prestador prestador)throws PersistenceException
	{
		return false;
	}
	
	public void save(Prestador prestador)throws PersistenceException
	{
		
	}
	
	public void update(Prestador prestador)throws PersistenceException
	{
		
	}
	
	public int count()throws PersistenceException
	{
		return 0;
	}

}
