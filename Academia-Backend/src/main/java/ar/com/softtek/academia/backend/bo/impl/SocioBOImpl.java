package ar.com.softtek.academia.backend.bo.impl;

import java.util.List;

import ar.com.academia.entities.SocioDTO;
import ar.com.academia.excepciones.BusinessException;
import ar.com.academia.excepciones.PersistenceException;
import ar.com.softtek.academia.backend.bo.SocioBO;
import ar.com.softtek.academia.backend.dao.SocioDAO;

public class SocioBOImpl implements SocioBO{
	
	private SocioDAO socioDAO;
	
	public SocioDAO getSocioDAO(){
		return socioDAO;
	}
	
	public void setSocioDAO(SocioDAO socioDAO){
		this.socioDAO = socioDAO;
	}
	
	public SocioDTO getSocioById(int id) throws BusinessException{
		try{
			SocioDTO dto = getSocioDAO().getSocioById(id);
			return dto;
		}catch(PersistenceException pe){
			throw new BusinessException(pe);
		}
	}
	
	public void actualizarSocio(SocioDTO socioDTO) throws BusinessException{
		try{
			getSocioDAO().update(socioDTO);
		}catch(PersistenceException pe){
			throw new BusinessException(pe);
		}
	}
	
	public boolean eliminarSocio(SocioDTO socioDTO) throws BusinessException{
		try{
			boolean bool = getSocioDAO().delete(socioDTO);
			return bool;
		}catch(PersistenceException pe){
			throw new BusinessException(pe);
		}
	}
	
	public boolean eliminarSocio(int id) throws BusinessException{
		try{
			boolean bool = getSocioDAO().delete(id);
			return bool;
		}catch(PersistenceException pe){
			throw new BusinessException(pe);
		}
	}
	
	public int cantidadSocios() throws BusinessException{
		try{
			int cant = getSocioDAO().count();
			return cant;
		}catch(PersistenceException pe){
			throw new BusinessException(pe);
		}
	}
	
	public void crearSocio(SocioDTO socioDTO) throws BusinessException{
		try{
			getSocioDAO().save(socioDTO);
		}catch(PersistenceException pe){
			throw new BusinessException(pe);
		}
	}
	
	public List<SocioDTO> getAllSocios() throws BusinessException{
		try{
			List<SocioDTO> lista = getSocioDAO().getAllSocios();
			return lista;
		}catch(PersistenceException pe){
			throw new BusinessException(pe);
		}
	}
	
	public List<SocioDTO> getSociosBySexo(String sexo) throws BusinessException{
		try{
			List<SocioDTO> lista = getSocioDAO().getBySexo(sexo);
			return lista;
		}
		catch(PersistenceException pe)
		{
			throw new BusinessException(pe);
		}
	}
}
