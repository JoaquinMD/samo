package ar.com.softtek.academia.backend.dao;

import ar.com.academia.entities.Prestador;

public interface PrestadorDAO extends GenericDAO<Prestador>{

}
