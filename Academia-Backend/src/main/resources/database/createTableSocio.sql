CREATE TABLE [dbo].Socios(
	NRO_AFILIADO	int
		identity(1,1)
		not null primary key,
	NOMBRE_Y_APELLIDO nvarchar,
	TIPO_DNI nvarchar NOT NULL,
	DNI nvarchar NOT NULL,
	DIRECCION nvarchar,
	TELEFONO nvarchar not null,
	MAIL nvarchar,
	FECHA_NACIMIENTO date,
	SEXO nvarchar,
	ESTADO_CIVIL nvarchar,
	CANTIDAD_HIJOS int,
	ID_PLAN int references [dbo].[PLAN](ID_PLAN)
);