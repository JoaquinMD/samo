CREATE TABLE TURNO(
	NRO_TURNO	int
		identity(1,1)
		not null primary key,
	NRO_AFILIADO int references [dbo].Socios(NRO_AFILIADO),
	FECHA_LLEGADA datetime,
	FECHA_ATENCION datetime,
	ID_PRESTADOR float references [dbo].[Prestadores](Id_Prestador),
	IMPORTE float,
	OBSERVACIONES nvarchar
);