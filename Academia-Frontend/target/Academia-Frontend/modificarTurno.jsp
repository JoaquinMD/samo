<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Modificar Turno</title>
</head>
<body>
<div class="wrapper">
		
		<div class="space"></div>
		
        <h1 class="titulo">Modificar turno</h1>
            
        <div class="contenedor">

		<s:form id="formulario" method="POST" action="/modificarTurno">
			<table>
			
			<tr>
				<td><s:textfield label="N� de Socio" type="number" name="turno.numeroSocio" readonly="true"></s:textfield></td>            
            </tr>
            <tr>
				<td><s:textfield label="Nombre y apellido"  name="turno.nombreYapellido" readonly="true"></s:textfield></td>            
            </tr>
            
            <tr>
            	<td><s:select label="Plan" name="socio.plan" list="#{'2210': '2210', '2310':'2310', '3310':'3310' }"></s:select></td>
            </tr>
             <tr>
            	<td><s:textfield label="Practica" name="turno.practica" readonly="true"></s:textfield></td>
            </tr>

            <tr>
            	<td><s:textfield label="Prestador" name="turno.prestador" readonly="true" ></s:textfield></td>
            </tr>
            <tr>
            	<td><s:textfield label="Fecha de Turno" name="turno.fechaTurno" type="date" format="dd/MM/yyyy hh:mm"></s:textfield></td>
            </tr>
             <tr>
            	<td><s:textfield label="Fecha y Hora de llegada" name="turno.llegadaTurno" type="date" format="dd/MM/yyyy hh:mm"></s:textfield></td>
            </tr>
            <tr>
            	<td><s:textfield label="Importe" name="socio.importe" ></s:textfield></td>
            </tr>
             <tr>
            	<td><s:textfield label="Observaciones" name="socio.observaciones" ></s:textfield></td>
            </tr>

			
			<tr>
			    <td>
			    <s:submit value="Confirmar" type="button"/>
			    </td>
			</tr>
			
		</table>
		
        </s:form>
		
		<button class="boton-volver" name="resetForm" onclick="location.href='listarSocios'">Volver</button>

        </div>

		</div>


</body>
</html>