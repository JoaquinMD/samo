<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Inicio - Socios</title>
		<link rel="stylesheet" href="css/socios.css" type="text/css"/>
	</head>
<body>
	<div class="container-index">
	
		<div class="wrapper">

            <h1 class="titulo">Socios</h1>
			
			<div class="menu">
            	<button class="nuevo-socio" onclick="location.href='nuevoSocio.jsp'">Nuevo socio</button>
			</div>
			
            	<table class="tabla-socios">
                	<tr>
                    	<th>Nro Socio</th>
                    	<th>Plan</th>
                    	<th>Nombre y Apellido</th>
                    	<th>Fecha de nacimiento</th>
                    	<th>Sexo</th>
                    	<th>Estado Civil</th>
                    	<th>DNI</th>
                    	<th>Telefono</th>
                    	<th>Mail</th>
                   	 	<th>Direccion</th>
                    	<th>Acciones</th>
                	</tr>
                                
               		<s:iterator value="socios">
                		<tr>
                			<td><s:property value="numeroSocio"/></td>
                			<td><s:property value="plan"/></td>
         	    	   		<td><s:property value="nombreYapellido"/></td>
         	    	   		<td><s:property value="fechaNacimiento"/></td>
            	    		<td><s:property value="sexo"/></td>
                			<td><s:property value="estadoCivil"/></td>
                			<td><s:property value="dni"/></td>
                			<td><s:property value="telefono"/></td>
                			<td><s:property value="mail"/>
                			<td><s:property value="direccion"/></td>
                			<td class="tabla-opciones">
                			<s:a action="/obtenerSocio"><s:param name="socio.id" value="id"/>
                        		<button class="modificar-socio">Modificar socio</button>
                        	</s:a>
                        	<s:a action="/eliminarSocio"> <s:param name="socio.id" value="id"/>
                        		<button class="eliminar-socio">Eliminar socio</button>
                        	</s:a>
                        
                   			</td>
                		</tr>
                	</s:iterator>
            	</table>

        	</div>
        	<div class="wrapper">

            <h1 class="titulo">Turnos</h1>
			
			<div class="menu">
            	<button class="nuevo-turno" onclick="location.href='nuevoTurno.jsp'">Nuevo Turno</button>
			</div>
			
            	<table class="tabla-socios">
                	<tr>
                    	<th>Nro Turno</th>
                    	<th>Nro Socio</th>
                    	<th>Nombre y Apellido</th>
                    	<th>Plan</th>
                    	<th>Fecha y hora llegada</th>
                    	<th>Fecha y hora atencion</th>
                    	<th>ID Prestador</th>
                    	<th>Nro Practica</th>
                    	<th>Importe</th>
                   	 	<th>Observaciones</th>
                    	
                	</tr>
                                
               		<s:iterator value="turnos">
                		<tr>
                			<td><s:property value="id"/></td>
                			<td><s:property value="socio.id"/></td>
         	    	   		<td><s:property value="socio.nombreYapellido"/></td>
         	    	   		<td><s:property value="socio.plan"/></td>
            	    		<td><s:property value="fechaHoraLlegada"/></td>
                			<td><s:property value="fechaHoraInicioAtencion"/></td>
                			<td><s:property value="prestador"/></td>
                			<td><s:property value="numeroPractica"/></td>
                			<td><s:property value="importe"/>
                			<td><s:property value="observaciones"/></td>
                			<td class="tabla-opciones">
                			<s:a action="/obtenerTurno"><s:param name="socio.id" value="id"/>
                        		<button class="modificar-turno">Modificar Turno</button>
                        	</s:a>
                        	<s:a action="/eliminarTurno"> <s:param name="turno.id" value="id"/>
                        		<button class="eliminar-turno">Eliminar Turno</button>
                        	</s:a>
                        
                   			</td>
                		</tr>
                	</s:iterator>
            	</table>

        	</div>
        
        </div>
</body>
</html>
