//Script socios

$(document).ready(function(){
    
    if($('#formulario_socio_estadoCivil').val() == 'casado'){
        $('.casado').show();
    }
    else{
        $('.casado').hide();
    }

    $('#formulario_socio_estadoCivil').change(function(){
        if($('#formulario_socio_estadoCivil').val() == 'casado'){
            $('.casado').show();
        }
        else{
            $('.casado').hide();
        }
    });
});