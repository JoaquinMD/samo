<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<html>
	<head>
		<title>Modificar socio</title>
		<link rel="stylesheet" type="text/css" href="css/socios.css"/>
		<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
		<script src="http://malsup.github.com/jquery.form.js"></script>
        <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
        <script src="scripts/script-socios.js" type="text/javascript"></script>
	</head>
	<body>
		
		<div class="wrapper">
		
		<div class="space"/>

        <h1 class="titulo">Modificar socio</h1>
        
        <div class="contenedor">

		<s:form id="formulario" method="POST" action="/modificarSocio">
			<table>
			
			<tr>
				<td><s:textfield label="N� de Socio" type="number" name="socio.numeroSocio" readonly="true"></s:textfield></td>            
            </tr>
            
            <tr>
            	<td><s:select label="Plan" name="socio.plan" list="#{'2210': '2210', '2310':'2310', '3310':'3310' }" ></s:select></td>
            </tr>

            <tr>
            	<td><s:textfield label="Nombre y Apellido" name="socio.nombreYapellido" ></s:textfield></td>
            </tr>

			<tr>
				<td>
					<s:radio label="Sexo" name="socio.sexo" list="#{'masculino':'Masculino', 'femenino':'Femenino', 'otros':'Otros' }"/>
           		</td>
            </tr>
            
			<tr>
				<td id="estadoCivil">
					<s:select label="Estado civil" name="socio.estadoCivil" list="#{'soltero': 'Soltero/a', 'casado':'Casado/a'}"></s:select>
				</td>
			</tr>
              
            <tr class="casado">
            	<td><s:textfield label="Nombre y Apellido Conyuge" name="socio.nombreConyuge"></s:textfield></td>
            </tr>		
            	
           	<tr class="casado">
            	<td><s:checkbox label="Tiene hijos" name="socio.tieneHijos"></s:checkbox></td>                                 			
			</tr>
            
            <tr>
                <td><s:textfield label="DNI" name="socio.dni"></s:textfield></td>
            </tr>

			<tr>
                <td><s:textfield label="Telefono" name="socio.telefono" type="number"></s:textfield></td>
            </tr>

			<tr>
			    <td><s:textfield label="Direccion" name="socio.direccion"></s:textfield>
			</tr>

			<tr>
				<td>
			    	<s:submit value="Modificar" type="button" />
			    </td>
			</tr>
			
		</table>
		
        </s:form>
		
		<button class="boton-volver" onclick="location.href='listarSocios'">Volver</button>

        </div>

		</div>
	</body>
</html>