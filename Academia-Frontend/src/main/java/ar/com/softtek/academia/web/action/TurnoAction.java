package ar.com.softtek.academia.web.action;

import java.util.List;

import ar.com.academia.entities.TurnoDTO;
import ar.com.academia.excepciones.ServiceException;
import ar.com.academia.services.TurnoService;

import com.opensymphony.xwork2.ActionSupport;

public class TurnoAction extends ActionSupport{
	TurnoService service;
	TurnoDTO turno;
	List<TurnoDTO> turnos;
	
	public String addTurno(){
		try
		{
			getService().add(turno);
			return SUCCESS;
		}
		catch(ServiceException e){
			return ERROR;
		}
	}
	
	public String editTurno(){
		try{
			getService().edit(turno);
			return SUCCESS;
		}catch(ServiceException e)
		{
			return ERROR;
		}
	}
	
	public String deleteTurno(){
		try{
			getService().removeById(getTurno().getId());
			return SUCCESS;
		}catch(ServiceException e)
		{
			return ERROR;
		}
	}
	

	
	public String obtenerTurno(){
		try{
			turno = getService().getById(turno.getId());
			return SUCCESS;
		}catch(ServiceException e)
		{
			return ERROR;
		}
	}
	
	public TurnoDTO getTurno(){
		return turno;
	}
	
	public void setTurno(TurnoDTO turno){
		this.turno = turno;
	}
	
	public List<TurnoDTO> getTurnos(){
		return turnos;
	}
	
	public TurnoService getService(){
		return service;
	}
	
	public void setService(TurnoService service){
		this.service = service;
	}
	

}
