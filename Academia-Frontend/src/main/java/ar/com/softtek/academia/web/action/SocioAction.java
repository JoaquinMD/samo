package ar.com.softtek.academia.web.action;

import java.util.*;

import ar.com.academia.entities.SocioDTO;
import ar.com.academia.excepciones.ServiceException;
import ar.com.academia.services.SocioService;

import com.opensymphony.xwork2.ActionSupport;

public class SocioAction extends ActionSupport {
	
	SocioService service;
	SocioDTO socio;
	List<SocioDTO> socios;
	
	public String addSocio(){
		try
		{
			getService().add(socio);
			return SUCCESS;
		}
		catch(ServiceException e){
			return ERROR;
		}
	}
	
	public String editSocio(){
		try{
			getService().edit(socio);
			return SUCCESS;
		}catch(ServiceException e)
		{
			return ERROR;
		}
	}
	
	public String deleteSocio(){
		try{
			getService().removeById(getSocio().getId());
			return SUCCESS;
		}catch(ServiceException e)
		{
			return ERROR;
		}
	}
	
	public String listSocios(){
		try{
			socios = getService().getAll();
			return SUCCESS;
		}catch(ServiceException e)
		{
			return ERROR;
		}
	}
	
	public String obtenerSocio(){
		try{
			socio = getService().getById(socio.getId());
			return SUCCESS;
		}catch(ServiceException e)
		{
			return ERROR;
		}
	}
	
	public SocioDTO getSocio(){
		return socio;
	}
	
	public void setSocio(SocioDTO socio){
		this.socio = socio;
	}
	
	public List<SocioDTO> getSocios(){
		return socios;
	}
	
	public SocioService getService(){
		return service;
	}
	
	public void setService(SocioService service){
		this.service = service;
	}
	
}
