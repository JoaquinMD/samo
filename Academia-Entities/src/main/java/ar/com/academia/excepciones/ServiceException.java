package ar.com.academia.excepciones;

public class ServiceException extends Exception{
	
	private Exception e;
	
	public ServiceException(Exception e) {
		setException(e);
	}
	
	public ServiceException(){
		
	}

	public Exception getException() {
		return e;
	}

	public void setException(Exception e) {
		this.e = e;
	}

}
