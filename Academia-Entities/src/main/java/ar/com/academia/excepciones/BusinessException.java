package ar.com.academia.excepciones;

public class BusinessException extends Exception{

	private Exception e;
	
	public BusinessException(){
		
	}
	
	public BusinessException(Exception e){
		this.e = e;
	}
	
	public Exception getException(){
		return e;
	}
	
	public void setException(Exception e){
		this.e = e;
	}
}
