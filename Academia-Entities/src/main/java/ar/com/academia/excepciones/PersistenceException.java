package ar.com.academia.excepciones;

public class PersistenceException extends Exception{
	
	Exception e;

	public Exception getException(){
		return e;
	}
	public void setException(Exception e){
		this.e = e;
	}
	
	public PersistenceException(Exception e){
		setException(e);
	}

}
