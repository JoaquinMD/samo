package ar.com.academia.entities;

import java.util.Calendar;

public class TurnoDTO {
	
	private int id;
	private Socio socio;
	private Calendar fechaHoraLlegada;
	private Calendar fechaHoraInicioAtencion;
	private String especialidad;
	private String fecha;
	private Prestador prestador;
	private int numeroPractica;
	private double importe;
	private String observaciones;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Socio getSocio() {
		return socio;
	}
	public void setSocio(Socio socio) {
		this.socio = socio;
	}
	public Calendar getFechaHoraLlegada() {
		return fechaHoraLlegada;
	}
	public void setFechaHoraLlegada(Calendar fechaHoraLlegada) {
		this.fechaHoraLlegada = fechaHoraLlegada;
	}
	public Calendar getFechaHoraInicioAtencion() {
		return fechaHoraInicioAtencion;
	}
	public void setFechaHoraInicioAtencion(Calendar fechaHoraInicioAtencion) {
		this.fechaHoraInicioAtencion = fechaHoraInicioAtencion;
	}
	public String getEspecialidad() {
		return especialidad;
	}
	public void setEspecialidad(String especialidad) {
		this.especialidad = especialidad;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public Prestador getPrestador() {
		return prestador;
	}
	public void setPrestador(Prestador prestador) {
		this.prestador = prestador;
	}
	public int getNumeroPractica() {
		return numeroPractica;
	}
	public void setNumeroPractica(int numeroPractica) {
		this.numeroPractica = numeroPractica;
	}
	public double getImporte() {
		return importe;
	}
	public void setImporte(double importe) {
		this.importe = importe;
	}
	public String getObservaciones() {
		return observaciones;
	}
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	
	
	

}
