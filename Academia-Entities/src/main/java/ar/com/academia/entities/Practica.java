package ar.com.academia.entities;

public class Practica {
	private int codigo;
	private String practica;
	public int getCodigo() {
		return codigo;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	public String getPractica() {
		return practica;
	}
	public void setPractica(String practica) {
		this.practica = practica;
	}
}
