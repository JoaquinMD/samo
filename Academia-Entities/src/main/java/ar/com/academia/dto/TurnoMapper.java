package ar.com.academia.dto;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import ar.com.academia.entities.Turno;
import ar.com.academia.entities.TurnoDTO;

public class TurnoMapper {
	
	public static TurnoDTO convertirTurnoaTurnoDTO(Turno turno)
	{
		TurnoDTO t=new TurnoDTO();
		t.setId(turno.getId());
		t.setSocio(turno.getSocio());
		t.setFechaHoraLlegada(turno.getFechaHoraLlegada());
		t.setFechaHoraInicioAtencion(turno.getFechaHoraInicioAtencion());
		t.setEspecialidad(turno.getEspecialidad());
		t.setFecha(turno.getFecha());
		t.setPrestador(turno.getPrestador());
		t.setNumeroPractica(turno.getNumeroPractica());
		t.setImporte(turno.getImporte());
		t.setObservaciones(turno.getObservaciones());
		
		return t;
	}
	
	public static Turno convertirTurnoDTOaTurno(TurnoDTO turnoDTO)
	{
		Turno t=new Turno();
		t.setId(turnoDTO.getId());
		t.setSocio(turnoDTO.getSocio());
		t.setFechaHoraLlegada(turnoDTO.getFechaHoraLlegada());
		t.setFechaHoraInicioAtencion(turnoDTO.getFechaHoraInicioAtencion());
		t.setEspecialidad(turnoDTO.getEspecialidad());
		t.setFecha(turnoDTO.getFecha());
		t.setPrestador(turnoDTO.getPrestador());
		t.setNumeroPractica(turnoDTO.getNumeroPractica());
		t.setImporte(turnoDTO.getImporte());
		t.setObservaciones(turnoDTO.getObservaciones());
		
		return t;
	}
	
	public static List<TurnoDTO> convertirListaTurnoaListaTurnoDTO(List<Turno> l)
	{
		List<TurnoDTO> lista = new ArrayList<TurnoDTO>();
		Iterator<Turno> it = l.iterator();
		while(it.hasNext())
		{
			TurnoDTO dto = TurnoMapper.convertirTurnoaTurnoDTO(it.next());
			lista.add(dto);
		}
		return lista;
	}
}
