package ar.com.academia.dto;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import ar.com.academia.entities.Plan;
import ar.com.academia.entities.PlanDTO;

public class PlanMapper {
	
	public static PlanDTO convertirPlanaPlanDTO(Plan plan)
	{
		PlanDTO p=new PlanDTO();		
		p.setId(plan.getId());
		p.setDescripcion(p.getDescripcion());
		return p;
	}
	
	public static Plan convertirPlanaDTOPlan(PlanDTO planDTO)
	{
		Plan p=new Plan();
		
		p.setId(planDTO.getId());
		p.setDescripcion(planDTO.getDescripcion());
		return p;
	}
	
	public static List<PlanDTO> convertirlistaPlanalistaPlanDTO(List<Plan> l)
	{
		List<PlanDTO> lista = new ArrayList<PlanDTO>();		
		Iterator<Plan> it = l.iterator();
		while(it.hasNext())
		{
			PlanDTO dto = PlanMapper.convertirPlanaPlanDTO(it.next());
			lista.add(dto);
		}
		return lista;
		
	}

}
