package ar.com.academia.dto;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import ar.com.academia.entities.Socio;
import ar.com.academia.entities.SocioDTO;

public class SocioMapper {

	public static SocioDTO convertSocioToDTO(Socio socio){
		SocioDTO socioDTO = new SocioDTO();
		socioDTO.setCantidadHijos(socio.getCantidadHijos());
		socioDTO.setDireccion(socio.getDireccion());
		socioDTO.setDni(socio.getDni());
		socioDTO.setEstadoCivil(socio.getEstadoCivil());
		socioDTO.setId(socio.getId());
		socioDTO.setNombreConyuge(socio.getNombreConyuge());
		socioDTO.setNombreYapellido(socio.getNombreYapellido());
		socioDTO.setNumeroSocio(socio.getNumeroSocio());
		socioDTO.setPlan(socio.getPlan());
		socioDTO.setSexo(socio.getSexo());
		socioDTO.setTelefono(socio.getTelefono());
		socioDTO.setTieneHijos(socio.isTieneHijos());
		socioDTO.setMail(socio.getMail());
		SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
		socioDTO.setFechaNacimiento(format.format(socio.getFechaNacimiento()));
		return socioDTO;
	}
	
	public static Socio convertDTOtoSocio(SocioDTO socioDTO){
		Socio socio = new Socio();
		socio.setCantidadHijos(
				(socioDTO.getCantidadHijos() != null)?socioDTO.getCantidadHijos():0
				);
		socio.setDireccion(socioDTO.getDireccion());
		socio.setDni(socioDTO.getDni());
		socio.setEstadoCivil(socioDTO.getEstadoCivil());
		socio.setId(
				(socioDTO.getId() != null)?socioDTO.getId():0
				);
		socio.setNombreConyuge(socioDTO.getNombreConyuge());
		socio.setNombreYapellido(socioDTO.getNombreYapellido());
		socio.setNumeroSocio(socioDTO.getNumeroSocio());
		socio.setPlan(socioDTO.getPlan());
		socio.setSexo(socioDTO.getSexo());
		socio.setTelefono(socioDTO.getTelefono());
		socio.setTieneHijos(socioDTO.isTieneHijos());
		socio.setMail(socioDTO.getMail());
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		
		try{
		socio.setFechaNacimiento(format.parse(socioDTO.getFechaNacimiento()));
		}catch(ParseException e)
		{
			socio.setFechaNacimiento(null);
		}
		
		return socio;
	}

	public static List<SocioDTO> convertSocioListToDTOList(List<Socio> lista) {
		List<SocioDTO> listaDTO = new ArrayList<SocioDTO>();
		Iterator<Socio> it = lista.iterator();
		while(it.hasNext())
		{
			SocioDTO dto = SocioMapper.convertSocioToDTO(it.next());
			listaDTO.add(dto);
		}
		return listaDTO;
	}
}
