package ar.com.academia.services;

import java.util.List;

import javax.jws.*;
import javax.xml.bind.annotation.XmlSeeAlso;

import ar.com.academia.entities.SocioDTO;
import ar.com.academia.excepciones.ServiceException;

@WebService(name="SocioServiceProxy")
@XmlSeeAlso({SocioDTO.class})
public interface SocioServiceProxy {
	
	@WebMethod(operationName="AddSocio")
	@WebResult(name="AddSocioResult")
	public int add(
			@WebParam(name="socio")
			SocioDTO socioDTO
			) throws ServiceException;
	
	@WebMethod(operationName="GetAllSocios")
	@WebResult(name="Socios")
	public List<SocioDTO> getAll() throws ServiceException;
	
	@WebMethod(operationName="GetSocioById")
	@WebResult(name="Socio")
	public SocioDTO getById(
			@WebParam(name="id")
			int id
			)throws ServiceException;
	
	@WebMethod(operationName="GetSociosBySexo")
	@WebResult(name="SociosBySexo")
	public List<SocioDTO> getBySexo(
			@WebParam(name="sexo")
			String sexo
			)throws ServiceException;
	
	@WebMethod(operationName="RemoveSocioById")
	@WebResult(name="RemoveSocioByIdResult")
	public boolean removeById(
			@WebParam(name="id")
			int id
			)throws ServiceException;
}
