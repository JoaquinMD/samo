package ar.com.academia.services;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;

import ar.com.academia.entities.PrestadorDTO;
import ar.com.academia.excepciones.ServiceException;

@WebService(name="PrestadorService")
@XmlSeeAlso({PrestadorDTO.class})
public interface PrestadorService {
	
	@WebMethod(operationName="AddPrestador")
	@WebResult(name="IdAddedPrestador")
	public int add(
			@WebParam(name="prestadorDTO")
			PrestadorDTO prestadorDTO
			)throws ServiceException;
	
	@WebMethod(operationName="EditPrestador")
	@WebResult(name="IsPrestadorEdited")
	public boolean edit(
			@WebParam(name="prestadorDTO")
			PrestadorDTO prestadorDTO
			)throws ServiceException;
	
	@WebMethod(operationName="RemovePrestadorById")
	@WebResult(name="RemovePrestadorResult")
	public boolean removeById(
			@WebParam(name="id")
			int id
			) throws ServiceException;
	
	@WebMethod(operationName="GetAllPrestadores")
	@WebResult(name="Prestador")
	public List<PrestadorDTO> getAll() throws ServiceException;
	
	@WebMethod(operationName="GetPrestadorById")
	@WebResult(name="Prestador")
	public PrestadorDTO getById(
			@WebParam(name="id")
			int id
			) throws ServiceException;

}
