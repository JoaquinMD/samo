package ar.com.academia.services;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;

import ar.com.academia.entities.TurnoDTO;
import ar.com.academia.excepciones.ServiceException;

@WebService(name="TurnoService")
@XmlSeeAlso({TurnoDTO.class})
public interface TurnoService {
	
	@WebMethod(operationName="AddTurno")
	@WebResult(name="IdAddedTurno")
	public int add(
			@WebParam(name="turno")
			TurnoDTO turno
			) throws ServiceException;
	
	@WebMethod(operationName="EditTurno")
	@WebResult(name="IsTurnoEdited")
	public boolean edit(
			@WebParam(name="turno")
			TurnoDTO turno
			)throws ServiceException;
	
	@WebMethod(operationName="RemoveTurnoById")
	@WebResult(name="IsRemoved")
	public boolean removeById(
			@WebParam(name="id")
			int id
			) throws ServiceException;
	
	@WebMethod(operationName="GetAllTurnos")
	@WebResult(name="Turno")
	public List<TurnoDTO> getByIdSocio(
			@WebParam(name="idSocio")
			int idSocio
			) throws ServiceException;
	
	@WebMethod(operationName="GetTurnoById")
	@WebResult(name="Turno")
	public TurnoDTO getById(
			@WebParam(name="id")
			int id
			) throws ServiceException;
}
